[brutal]: #title "Arch8: Archive for 8-bit fonts"

Welcome to _Arch8_.
The archive is under construction,
for now please browse our temporary exhibit from the ZX Spectrum
collection:

- [![Doomdark's Revenge](media/doomdark.png)](doomdark/) a fantasy strategy game
- [![Quazatron](media/quazatron.png)](quazatron/) a robot combat and upcycling game
- [![Spellbound](media/spellbound.png)](spellbound/) a puzzle and magic adventure game
- [![Time-Gate](media/timegate.png)](timegate/) a MICR-inspired font
- [![TLL](media/tll.png)](tll/) a jet fighter training game

Thank you.

∎
