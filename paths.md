# A short note about paths through the collection

We use _path_ to mean a curated collection of items,
probably sharing a theme, a topic, or something else
that unites them and brings them together.

(_path_ is chosen, partly because of the pun: it's a term of art
in describing the outlines that make up the shapes of glyphs in
fonts)

We can describe a path by a sequence of items.
A path is necessarily directed.
A path may loop and visit an item more than once,
in which case there will be a choice of exits from that item,
possible we can label the exit My Tour 1 and My Tour 2 or
similar (or we can create another path to describe the loop).

Each item is a node or a web page.

Each web page can have navigation links to the next and previous
item on each path.
Hopefully there won't be too many paths and it won't be
confusing.

Like font outlines, paths are circular is the sense that the
final node is succeeded by the first node
(the first node should not be listed at the end).

Ideally this can be implemented without any state, cookies, or
JavaScript, the user is responsible for remembering which path
they are intending to follow.
And if they forget, well, they may make a serendipitous discovery.

# END
