[brutal]: #curator "David Jones"

# Time-Gate (ZX Spectrum, 1983)

A MICR-inspired font with more conventional weight distribution.

![Time-Gate](gal-f0-t0-s48.png)
![AKU4 Sample](gal-f0-t1-s48.png)

Note the compact 5-pixel high design,
probably only possible because only capitals are drawn.

## ∎
