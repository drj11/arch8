# Ranarama

Conversion to flat image ok;
font @ 27088

The font @ 27088 starts with Double Up, Double Down, Space, and
is followed by some runes.

The runes in Ranarama are actual runes
(mostly Elder Futhrark, but apparently some Tolkien):

- ᛟ U+16DF RUNIC LETTER OTHALAN ETHEL O
- ᚦ U+16A6 RUNIC LETTER THURISAZ THURS THORN
- ᚾ U+16BE RUNIC LETTER NAUDIZ NYD NAUD N
- ᛞ U+16DE RUNIC LETTER DAGAZ DAEG D
- ᛊ U+16CA RUNIC LETTER SOWILO S
- ᛯ U+16EF RUNIC TVIMADUR SYMBOL
- ᛳ U+16F3 RUNIC LETTER OO
- ᛓ U+16D3 RUNIC LETTER SHORT-TWIG-BJARKAN B

The alphabet generally is "runic" in form, with some letters
being more or less direct versions of runes:

- A = ᚨ U+16A8 RUNIC LETTER ANSUZ A
- T = ᛏ U+16CF RUNIC LETTER TIWAZ TIR TYR T
- Y = ᛘ U+16D8 RUNIC LETTER LONG-BRANCH-MADR M
- Z = mirrored ᛇ U+16C7 RUNIC LETTER IWAZ EOH

# END
