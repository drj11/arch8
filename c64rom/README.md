# Commodore 64 ROM fonts

The `.bin` files are from https://www.pagetable.com/c64ref/charset/.
There are several variants available.

Unless otherwise noted, the C64 versions are referred to.

The non-ASCII box-drawing and other graphic characters available in the
character set are a well-loved feature of what is together known as PETSCII.

## Design Notes

A conventional 8×8 pixel grid is used.

- cap-height 7 pixels
- ascenders 6 pixels
- descenders 6 pixels
- x-height 5 pipxels
- but bowls with ascender/descender use 4 pixel bowl: b d p q;
  similarly **g** and **y**

To display better on low-res TV sets,
a vertical stroke width of 2 pixels is used;
horizontally an isolated single pixel rarely appears
(but does appear in @ £ % and so on).

# END