#!/bin/sh

set -e

Base=c64_us_upper

( echo 'P4 8 1024'; cat $Base.bin ) |
  pripamtopng > $Base.png

cuttar $Base.png |
vec8 -control control-$Base.csv - > $Base.svg

ttf8 -control control-$Base.csv -dir $Base $Base.svg
f8name -dir $Base -control control-$Base.csv
assfont -dir $Base

plak --order code $Base.ttf |
  kitty icat
