#!/bin/sh

set -e -u

: "sampler
Produces a sample of a font as a series of PNG files.
This script is embedded and isn't really intended to be used outside of
the arch8 project, and may embody some non-portable assumptions.
"

usage () {
  printf "sh sampler.sh fontfile.ttf\n"
}

if [ $# = 0 ]
then
  usage
  exit
fi

# Find arch8 project
ARCH8=${PWD%arch8*}arch8

PATH=$ARCH8/fontgal:$PATH

Font=$1
true < "$Font"
FontDir=$(dirname "$Font")

fontgal -text '$FontName' -text '$GalSample' -size 48 "$Font"

# If the font has plaque in index.html it is assumed to be
# generated from index.md.
# So here we generate HTML for the sampler and write to index.html,
# unless index.md exists.

true 2> /dev/null < index.md ||
for a in gal-*.png
do
cat <<EOH
<img src="$a"></img>
EOH
done > index.html
