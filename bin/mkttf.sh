#!/bin/sh

set -e

Base=$(basename "$PWD")

for f in $(ls "control-$Base"*.csv)
do

g=${f%.csv}
Base=${g#control-}

cuttar "$Base".png |
vec8 - > "$Base".svg
ttf8 -control "$f" -dir "$Base" "$Base".svg
f8name -dir "$Base" -control "$f"
f8dsig -dir "$Base"
assfont -dir "$Base"

gnome-font-viewer "$Base".ttf
done
