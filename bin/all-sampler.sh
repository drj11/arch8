#!/bin/sh

# Script generally only intended to work when run
# at the root of the arch8 project.

for a in */*.ttf
do
  echo ${a%/*}
done | uniq |
while read -r a
do
  ( cd "$a"
    sh ../bin/sampler.sh *.ttf
  )
done
