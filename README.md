# Arch8

This is the source to the Arch8 project.

`bin/` contains a few binaries (scripts, actually) that make the
TTF files and PNG sampler cards.

The most likely command to rebuild the HTML is:

    brutal . */.

## Deploying

Typically this site is deployed as an “app” of
home.octetfont.com, by which i mean a subdirectory.

This is controlled by the
[gitlab CI job for octetfont.com](https://gitlab.com/drj11/octetfont.com/-/pipelines).
The pipeline will be run whenever a change to octetfont.com is pushed,
but can also be run “by hand” from the gitlab control panel.


## Populate `media/`

To create graphics in `media/`:

    hb-view --margin 10 --font-size 32 blah/*.ttf blahgame > media/blah.png

This create graphics that are 52 pixels high, but the existing
ones are 53.
I'm not going to care about that.

## Create a TTF

Most TTF files can be made from the PNG files by using the
`bin/mkttf.sh` script.
A few (currently [quazatron](quazatron) and [tll](tll)) require
a specialised script which is available in their directory.

The TTF files are in the `git` repo,
so don't generally need rebuilding.

## Create a sample card

From the root directory of this arch8 repo, run

    sh bin/all-sampler.sh

(this uses the `bin/sampler.sh` script).

Two PNG files are generated:
one for the font title set in the font;
the other is an alphabet (or other sample text) set in the font.
The TTF file can control the sample text used (name id 32008).

# END
