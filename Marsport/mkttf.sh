#!/bin/sh

set -e

Base=$(basename "$PWD")

for f in $(ls "$Base"*.png)
do

Base=$(echo "$f" | sed 's/-font.*//')

Cutopt=
if [ "$Base" = Marsport@47956 ]
then
    Cutopt="-h=8 -w=4"
fi

cuttar $Cutopt "$f" |
vec8 - > "$Base".svg
ttf8 -control control-"$Base".csv -dir "$Base" "$Base".svg
f8name -dir "$Base" -control control-"$Base".csv
f8dsig -dir "$Base"
assfont -dir "$Base"

gnome-font-viewer "$Base".ttf
done
