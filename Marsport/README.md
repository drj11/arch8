# Marsport

There are two fonts in Marsport:

The one higher in memory is the regular font.
A round font with contrast, with add hints of unusual MICR-like
weight placement.
Quite refreshing for the genre.
Repertoire is lowercase, plus a few more symbols.

The lower one is 4 pixels wide; not currently processable.
Repertoire is numbers and uppercase.

# END
