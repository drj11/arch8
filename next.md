# What Next

Well, i just discovered the tag « Utility: Fonts/UDGs » on
https://spectrumcomputing.co.uk/list?genre_id=50

- Heavy on the Magick https://spectrumcomputing.co.uk/entry/2274/ZX-Spectrum/Heavy_on_the_Magick
  good lettering and one of the 4-pixel fonts
- Klimax https://spectrumcomputing.co.uk/entry/2713/ZX-Spectrum/Klimax
- Knight Driver https://spectrumcomputing.co.uk/entry/2715/ZX-Spectrum/Knight_Driver
  pretty early
- Marsport https://spectrumcomputing.co.uk/entry/3040/ZX-Spectrum/Marsport
  rounded sci-fi with contrast
- Technician Ted https://spectrumcomputing.co.uk/entry/5160/ZX-Spectrum/Technician_Ted
- Astroclone
  https://spectrumcomputing.co.uk/entry/299/ZX-Spectrum/Astroclone
  Nice Data 70 font
- Avalon https://spectrumcomputing.co.uk/entry/348/ZX-Spectrum/Avalon
  Nice semi-uncial; same as Dragontorc?
- Uridium https://spectrumcomputing.co.uk/entry/5525/ZX-Spectrum/Uridium
  Can't tell if treatment or font; good lettering on title screen
- Zynaps https://spectrumcomputing.co.uk/entry/5890/ZX-Spectrum/Zynaps
  great game, great lettering; numbers only?
- Firelord https://spectrumcomputing.co.uk/entry/1775/ZX-Spectrum/Firelord
  good numbers; what font?
- Heavy Metal https://spectrumcomputing.co.uk/entry/2273/ZX-Spectrum/Heavy_Metal
  commendable stencil effort; a US Gold signature?; Charette?
- Wizard's Lair https://spectrumcomputing.co.uk/entry/5709/ZX-Spectrum/Wizards_Lair
  Atari / Quiz Game font? Great title screen lettering
- Ski Aquático https://spectrumcomputing.co.uk/entry/38808/ZX-Spectrum/Ski_Aqu%C3%A1tico
  May only have limited letters; very early
- Shooting Gallery https://spectrumcomputing.co.uk/entry/36151/ZX-Spectrum/Shooting_Gallery
  mediocre rectilinear computer font
- The Thompson Twins Adventure https://spectrumcomputing.co.uk/entry/7104/ZX-Spectrum/The_Thompson_Twins_Adventure
  Good bold sans; like Spellbound?
- 64 Columns https://spectrumcomputing.co.uk/entry/16953/ZX-Spectrum/64_Columns
  font on condensed body for 64 columns of text; it's terrible,
  but a good effort given the constraints
- Double Height Characters https://spectrumcomputing.co.uk/entry/17922/ZX-Spectrum/Double_Height_Characters
  says double-height, but the screenshot is not; crappy boxy
  scifi font
- Pubblicità https://spectrumcomputing.co.uk/entry/28065/ZX-Spectrum/Pubblicit%C3%A0
  An advert with some nice lettering
- 42 Columns https://spectrumcomputing.co.uk/entry/16941/ZX-Spectrum/42_Columns
  the Sinclair ROM font, with 1 pixel negative tracking. I feel
  like a font criminal just typing this in.
- 20 Character Set Fonts https://spectrumcomputing.co.uk/entry/19244/ZX-Spectrum/20_Character_Set_Fonts
  From the screenshot we can see commendable versions of
  Futura Black, a Stencil, and maybe a Data 70 / MICR
- The 8bit Font Collection https://spectrumcomputing.co.uk/entry/25364/ZX-Spectrum/The_8bit_Font_Collection
  Intriguing title and date (2007), but no screenshots
- Alfabetos https://spectrumcomputing.co.uk/entry/38447/ZX-Spectrum/Alfabetos
  Some more unusual designs
- CarSet https://spectrumcomputing.co.uk/entry/37385/ZX-Spectrum/CarSet
  outstanding attempt at conected script with additional French
  support
- CharacterSets https://spectrumcomputing.co.uk/entry/23104/ZX-Spectrum/Character_Sets
  Biform uncial, a wonky fantasy, and some more linear sans
- Characterset Demo https://spectrumcomputing.co.uk/entry/42025/ZX-Spectrum/Characterset_Demo
  The demo font is extremely rough-edged horror style
- DesktopFZX https://spectrumcomputing.co.uk/entry/28181/ZX-Spectrum/DesktopFZX
  2013. Einar Saukas seems to be doing good work. Italic /
  connected / accents; converted from Desktop
  https://spectrumcomputing.co.uk/entry/0016682A from the Czech
  publishers Proxima Software; explains the accent selection
- DesktopKud1FZX https://spectrumcomputing.co.uk/entry/28183/ZX-Spectrum/DesktopKud1FZX
  Data 70 with accents!
  Converted from Klub uživatelů Desktopu 1
  https://spectrumcomputing.co.uk/entry/0023578
- Even More Character Sets https://spectrumcomputing.co.uk/entry/23161/ZX-Spectrum/Even_More_Character_Sets
  Futura Black and Stencil again
- Font Editor
  https://spectrumcomputing.co.uk/entry/21464/ZX-Spectrum/Font_Editor
  Cyrillic
- Fonts https://spectrumcomputing.co.uk/entry/11714/ZX-Spectrum/Fonts
  another selection of a dozen or so, including Futura Black and
  Stencil; and an attempt at Bombere!
- Font Editor and Script designer https://spectrumcomputing.co.uk/entry/14728/ZX-Spectrum/Font_Editor_Script_Designer
  good uncial, and a greek! Also a hilarious connected / fantasy
- FZX https://spectrumcomputing.co.uk/entry/28171/ZX-Spectrum/FZX
  appears to be a font format used by Einar Saukas (see
  elsewhere)
- IlluminDemo https://spectrumcomputing.co.uk/entry/23126/ZX-Spectrum/IlluminDemo
  semi-uncial with drop caps.
- Letaset https://spectrumcomputing.co.uk/entry/11847/ZX-Spectrum/Letaset
  versions of Pump and Stop (as Odysey) and various others,
  including a Stencil and a shadow
- Typeset https://spectrumcomputing.co.uk/entry/12203/ZX-Spectrum/Typeset
  12 fonts, greek, hebrew, blackletter / split-serif
- Zeichen https://spectrumcomputing.co.uk/entry/9008/ZX-Spectrum/Zeichen
  connected script. maybe german?
- Enciclopedia Bompiani - Filosofia https://spectrumcomputing.co.uk/entry/39174/ZX-Spectrum/Enciclopedia_Bompiani-Filosofia
  serifed roman
- O Globo da Luz https://spectrumcomputing.co.uk/entry/38495/ZX-Spectrum/O_Globo_da_Luz
  Fantasy with uneven bold / blackboard-bold effect
- Rendition https://spectrumcomputing.co.uk/entry/37143/ZX-Spectrum/Rendition
  semi-uncial from Spain

- Super Video https://spectrumcomputing.co.uk/entry/28063/ZX-Spectrum/Super_Video
  A system for using REMs to create fancy text.
  Excellent conversion of Magnetic Ink (not Moore Computer as i
  first thought). Converted in arch8/SuperVideo; could be more.

- Magnetron
  https://spectrumcomputing.co.uk/entry/2986/ZX-Spectrum/Magnetron
  same as Quazatron?


- Pyjamarama nice font, can't extract from tzx, try snap

- Ranarama https://spectrumcomputing.co.uk/entry/4022/ZX-Spectrum/Ranarama
  Largely done. Some vert required

# END
