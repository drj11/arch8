[brutal]: #curator "David Jones"

# Doomdark's Revenge

Doomdark's Revenge (ZX Spectrum, 1984).
A very good round font that contributes to the fantasy setting
of the game.

![Doomdark's Revenge](gal-f0-t0-s48.png)
![Alphabet](gal-f0-t1-s48.png)

One of my favourite games, and one of my favourite fonts on the 
ZX Spectrum.
Slightly unusual to see matching sets of double quotes,
but justified for a game displaying a lot of text.

The font has 7 pixels for cap-height,
5 pixel x-height with 2 pixel ascenders.
This is a common figuration for 8-pixel fonts but means
that descenders have only 1 pixel, and they clash with the top
pixel of capital letters, ascenders, and numbers.
Doomdark's revenge has one little trick to help with this a bit:
The lower-case g is modelled without a closed bowl and
with just a tick of a descender.


## END
