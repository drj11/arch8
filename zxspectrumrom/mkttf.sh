set -e

Base=zxspectrumrom

cuttar -w 16 "${Base}-font.png" |
  vec8 - > "${Base}.svg"
ttf8 -control control-"${Base}".csv -dir "${Base}" "${Base}".svg
f8name -dir "${Base}" control-"${Base}".csv
f8dsig -dir "${Base}"
assfont -dir "${Base}"
