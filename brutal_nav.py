# brutal plugin
# Intended to be used with brutal, gitlab.com:drj11/brutal

import os

"""
This plugin reads nav.html and prepends it to info["body"].
"""

def brutal_add_nav(info):
    dir = os.path.dirname(info["html_name"])
    nav_file = os.path.join(dir, "nav.html")
    try:
        with open(nav_file) as r:
            info["body"] = r.read() + info["body"]
    except FileNotFoundError:
        return
