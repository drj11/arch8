# Super Video

Super Video is a magazine cover tape program. https://spectrumcomputing.co.uk/entry/28063/ZX-Spectrum/Super_Video

It contains several fonts, one of which is an adaptation of
Magnetic Ink https://fontsinuse.com/typefaces/115586/magnetic-ink

`snaptopng` can process the tzx file.

Magnetic Ink begings at offset 4080 but in a slightly unusual
format:

Each character is 9 bytes:

- 1 byte: ASCII position
- 8 bytes: graphics; top to bottom LSBit on right

# END
