#!/bin/sh

set -e

Base=$(basename "$PWD")

for f in "$Base"*.png
do
  Base=$(echo "$f" | sed 's/-font.*//')
  mkdir -p "$Base"-tile
  cuttar "$f" | (cd "$Base"-tile; tar xf -)
done

rm tll@21079-tile/*3[89].png
rm tll@21079-tile/*[4-9]?.png
rm tll@17577-tile/*1[1-9].png
rm tll@17577-tile/*[2-9]?.png

vec8 */*.png > tll.svg
ttf8 -control control-tll.csv -dir tll tll.svg
f8name -dir tll control-tll.csv
f8dsig -dir tll
assfont -dir tll

gnome-font-viewer tll.ttf
