[brutal]: #curator "David Jones"

# TLL aka Tornado Low Level

TLL (ZX Spectrum, 1984)

![TLL](gal-f0-t0-s48.png)
![TLL](gal-f0-t1-s48.png)

The main font is inspired by Broadway.
TLL makes very effective use of the 8 pixel monospace
restriction by using extreme contrast to fill out the square and
give impression of speed.

TLL has two fonts, one short used for scoring in the game:
It only has 0-9, S, H, space, and a possible punctuation character.
The main font, used for the game select page UI, is still
limited, but has A to Z, space, 0 to 9, /, and a possible full square.

In the sample above, the scoring numerals are on the bottom line.

## Technical notes

The larger font @21079 is used for the "UI".

The font @17577 has only numerals
and is used for the main game (for scores).

I have combined these two fonts into one, putting the game
numerals at U+1D7F6 (MATHEMATICAL MONOSPACE DIGIT ZERO),
and the other glyphs at their more usual ASCII positions.
Note TLL has a reduced glyph set and appears in memory with a
non-standard ordering: letters, space, numbers.
And in one set the numbers go 0 to 9, the other goes 1 to 9, 0.
That tripped me up.

## ∎
