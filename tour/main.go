package main

import (
	"encoding/csv"
	"flag"
	"fmt"
	"html"
	"log"
	"os"
	"path/filepath"
)

type Exit struct {
	source, target string
}

func main() {
	flag.Parse()

	file_name := flag.Arg(0)
	r, err := os.Open(file_name)
	if err != nil {
		log.Fatal(err)
	}
	csv_reader := csv.NewReader(r)

	tour_map := TourMap(csv_reader)

	nodes := MapNodes(tour_map)

	for _, node := range nodes {
		err = WriteNavExits(node, tour_map)
		if err != nil {
			log.Fatal(err)
		}
	}

	for k, v := range tour_map {
		fmt.Println(k, v)
	}
}

func TourMap(r *csv.Reader) map[string][]string {
	path := map[string][]string{}

	rows, err := r.ReadAll()
	if err != nil {
		log.Fatal(err)
	}
	for _, row := range rows {
		if row[0][0] == '#' {
			continue
		}
		path[row[0]] = append(path[row[0]], row[1])
	}

	return path
}

// Return the visited nodes as a unduplicated slice
func MapNodes(tour_map map[string][]string) []string {
	set := map[string]struct{}{}
	for _, v := range tour_map {
		for _, node := range v {
			set[node] = struct{}{}
		}
	}

	nodes := []string{}
	for node, _ := range set {
		nodes = append(nodes, node)
	}

	return nodes
}

func PathExits(node string, path []string) []Exit {
	exits := []Exit{}
	for i, item := range path {
		if item == node {
			var exit Exit
			if i == len(path)-1 {
				exit = Exit{node, path[0]}
			} else {
				exit = Exit{node, path[i+1]}
			}
			exits = append(exits, exit)
		}
	}
	return exits
}

// For the given node, discover its exits using the tour_map
// and write them into the file node/nav.html
func WriteNavExits(node string, tour_map map[string][]string) error {
	w, err := os.OpenFile(filepath.Join(node, "nav.html"),
		os.O_CREATE|os.O_TRUNC|os.O_WRONLY, 0644)
	if err != nil {
		return err
	}
	for path_name, path := range tour_map {
		exits := PathExits(node, path)
		for _, exit := range exits {
			fmt.Fprintf(w, "<a href='../%s/'>next on %s</a>\n", exit.target, html.EscapeString(path_name))
		}
	}

	return nil
}
