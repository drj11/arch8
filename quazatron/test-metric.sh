#!/bin/sh
set -e
Widths=$(seefont metrics quazatron.ttf |
  awk -F, '{print $2}' | sort | uniq -c)
if [ "$(printf '%s\n' "$Widths" | wc -l)" != 2 ]
then
    echo 2>/dev/null "Expected 2 widths, but got: " "$Widths"
    exit 4
fi
exit 0
