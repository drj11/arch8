#!/bin/sh

set -e

In=quazatron@3216-font.png

mkdir -p t

cuttar "$In" | (cd t
  tar xf -
  weft *_22.png *_38.png > m.png
  weft *_32.png *_40.png > w.png
  mv m.png *_22.png
  mv w.png *_32.png
  rm *_[4-9]?.png
)
vec8 t/*.png > quazatron.svg
ttf8 -control control-quazatron.csv -dir quazatron quazatron.svg
f8name -control control-quazatron.csv -dir quazatron
f8dsig -dir quazatron
assfont -dir quazatron

gnome-font-viewer quazatron.ttf
