[brutal]: #curator "David Jones"

# Quazatron

Quazatron (ZX Spectrum, 1986) has a font that is
a redrawing of Motter Tektura (1971).

![Quazatron](gal-f0-t0-s48.png "Quazatron font")
<br>
![Alphabet](gal-f0-t1-s48.png "Alphabet in Quazatron font")

It has several unusual features:

- caps that have descenders (A, B, E, etc)
- wild numerals that have ascenders and descenders in non-standard arrangements
- not monospace in this 8-bit version (the M and W are double width).

Compared to Motter Tektura,
the G is redrawn (and improved),
the S is shifted down (so that it descends) and is drawn completely.
In Motter Tektura many of the numerals are
deliberately clipped at the top;
this version draws them in full, but making them very tall in the process.

All 8 pixel rows are used, sometimes all 8 in one glyph.
The result is text that clashes horribly with the row above and below.
Quazatron the game "fixes" this by leaving a blank row in
between rows with text,
effectively putting 8 pixels of leading on an 8 pixel body.

## Technical notes

The basic Quazatron character set is unusual:

- It does not use the ASCII ordering.
- Only the glyphs for 0-9, A-Z, dot, space are present.
- The M and W glyphs are double wide and spread over 2 8x8 tiles.

The 2 8x8 tiles for M and W are not contiguous.
M is tile 22 and tile 38;
W is tile 32 and tile 40.
In each case the left-hand tile is in the usual position in the
alphabet.

The scripted process to make the font joins the
images to make `M` and `W` using the `weft` tool.


## Other

It's possible that Paradroid (same author) has an extended
version of this font that includes lower case:

https://fontsinuse.com/uses/20184/paradroid

## END
