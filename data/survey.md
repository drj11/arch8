# Survey of Spectrum games.

using https://spectrumcomputing.co.uk/list?yearfrom=1982&yearto=1982&machine_id=3&pg=4

1982 nothing up to S
Star Trek (1982), MICR, looks quite good
Time-Gate (1982), already got
Adventure Island (1983), not clear if there is a full font
Alchemist, good magic-aura
Autostopista Galáctico (1983), might not be full
Bear Bovver, just numbers?
The Birds and The Bees (1983), kinda wirey and blocky like vector drawing
Cannon Ball, just numbers?
Car Journey (1983), is that a prop font?
Circus (1983), actually can't tell if this is custom
Confusion (1983), MICR
Crazy Golf (1983), Custom but not very good
3D Defenda (1983), MICR
Defender (1983-Sinclair Programs), kinda vector based and i like it.
Defender (1983-Popular Computing Weekly), MICR
The Detective (1983), little bit roundy
DICE (1983-EMM Software), big and chunky
Dimension Destructors, small and vectory
Double Trouble, prop?
Driller Tanks (1983), Arcadey?
Elektro Storm (1983), 5 bar MICR
Frogger (1983), not great, but ok
Game Designer 9(1983), MICR (same as timegate?)
Ghost Town (1983), nice bodoni
Gobstopper (1983), kinda scifi
The Golden Batton (1983), (Mysterious Adventures) is this custom?
Golfing World (1983), more MICR than Golf
Halls of the Things (1983), custom?
Headbangers Heaven (1983), bold and arcadey
Here Comes The Sun (1983), 5 bar MICR
House of the Living Dead (1983), good effort?
Hunchy (1983), terrible looking scrolly job
Itasundorious (1983), isn't this same as the other Hudon Soft game?
Jail Break (1983), vector MICR
Killer Kong (1983), excellent shadow effect but may only be numbers
Kong (1983), bold
Laser Zone (1983), MICR
Last Sunset for Lattica, Roundy
Learn to Read 1 (1983), Has a large custom pixel font maybe Helvetica?
Leopard Lord (1983), nice adventure game font
