[brutal]: #curator "David Jones"

# Spellbound

Spellbound (ZX Spectrum, 1985)

![Spellbound](gal-f0-t0-s48.png)
![Spellbound](gal-f0-t1-s48.png)

An excellent execution of a bold san-serif with little
elaboration.
Many letters are particularly fine (A I L O U V), others
suffer a little from having been squeezed into a 7-pixel
cap-height:
The E and S have a too-narrow middle horizontal stroke.
Note the Copyright symbol, which uses a reverse video effect.

The X is oddly distorted.
Normally I might put this down to X
not appearing very often, so not having much attention paid to it.
But in this game, it appears in the EXAMINE command,
which is quite frequent!

This font is also used in the other Magic Knight games, Finders
Keepers, and Knight Tyme.


## Capture Notes

Spellbound was one of the first fonts I used when I was first
developing the Font 8 tools.

## END
