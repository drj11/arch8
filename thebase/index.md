[brutal]: #curator "David Jones"

# The Base (ZX Spectrum, 1988)

![The Base](gal-f0-t0-s48.png)

Only the heavy version @1102 is shown.

![Alphabet](gal-f0-t1-s48.png)

## Capture Notes

This game seems to offer several fonts.
offset 334, immediately followed by 1102, and 1870.

@334 is well proportioned upright suitable for large amounts of
screen text.

@1102 is a heavier version, but the capitals are out of
proportion to the lower case.

@1870 is a more digital style, possibly reminiscent of MICR or
Etch-a-Sketch.

## ∎
