# Font Gallery

    fontgal [-size 40,80,120] [-text "glyph job" -text Quartz ARIAL.TTF] COURI.TTF

`fontgal` renders text into images, using the font files specified.
It wil loop over all Font Files given (as arguments),
all texts (multiple `-text` flags are allowed), and
all sizes (comma separated after the `-size` flag),
producing one file for each combination.

The output file is named:

   gal-fN-tX-sH.png

where N is the index of the font file (0, 1, 2...);
X is the index of the `-text` flag;
H is the size (the size, not the index);

The `-size` flag is optional.

The `-text` flag is optional and default to `$GalSample`,
which is a sample text derived from either the font file itself
or an internal default (see below).

## Environment Substitution

Environment variables are interpolated into the string,
using the usual `${var}` or `$var` convention
(internally, the Go function
[`os.ExpandEnv`](https://golang.org/pkg/os/#ExpandEnv) is used).
If you want to avoid that, put U+2060 after each `$`.

`fontgal` adds some predefined environment variables:

`$SampleAFK` a 10x8 character sample display.

`$SampleMin` a 4 row sample display of Capitals and Numerics only.

There are other `$Sample` variables; consult the source.

`$FontName` is the name of the font (Name ID 4)
extracted from the font file
(the first such name is taken regardless of its language code,
but it is rare for fonts to have this in more than one language).

`$GalSample` is the default text output (when no `-text` flag is
supplied).
It is taken from the font file itself;
it is the text of a name table entry with Name ID 32008.
This is used by the Arch 8 project to set custom samples for
each font.
If the font file has no Name ID 32008 then the same text as
`$SampleAFK` is put in this environment variable.

## Research

The current version of `fontgal` uses the HarfBuzz tool `hb-view`
to convert text to a PNG, using a TTF file.

Some alternatives:
- was previously using Pango, `pango-view`
- was previously using Image Magick
- ttf2png, suggested here: https://stackoverflow.com/a/27237795/242457
- otf2bdf may have some useful components? http://sofia.nmsu.edu/~mleisher/Software/otf2bdf/


## Installation

It's a Go program.
The dependency on opentype package currently requires that the
package is available at `../../opentype`;
which it will be if you clone `opentype` adjacent to this repo.

# END
