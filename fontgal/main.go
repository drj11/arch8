package main

// Font gallery
// From a TrueType Font file, produce a number of PNG gallery images.
// Actually calls `hb-view` from HarfBuzz to the do the work.

import (
	"example.com/opentype/opentype"
	"flag"
	"fmt"
	"log"
	"os"
	"os/exec"
	"strconv"
	"strings"
)

type Texts []string

func (p *Texts) String() string {
	return strings.Join(*p, "\x1A")
}
func (p *Texts) Set(s string) error {
	*p = append(*p, s)
	return nil
}

var texts Texts

var sizesp = flag.String("size", "24", "comma separated sizes")

var outputp = flag.String("output", "", "output file (only valid for single output)")

func main() {
	flag.Var(&texts, "text", "text to convert (multiple flags allowed)")
	flag.Parse()

	if flag.NArg() <= 0 {
		log.Fatal("at least one font file is required")
	}
	sizes := strings.Split(*sizesp, ",")

	if texts == nil {
		texts = []string{"$GalSample"}
	}

	if *outputp != "" {
		if flag.NArg() > 1 ||
			len(sizes) > 1 ||
			len(texts) > 1 {
			log.Fatal("-output can only be used if there is a single output")
		}
	}

	AddSamples()

	for fi, font := range flag.Args() {

		AddEnvFontNames(font)

		for _, sz := range sizes {
			size, err := strconv.Atoi(sz)
			if err != nil {
				log.Fatal(err)
			}

			for ti, text := range texts {
				out_name := fmt.Sprintf("gal-f%d-t%d-s%d.png", fi, ti, size)
				if *outputp != "" {
					out_name = *outputp
				}

				expanded := os.ExpandEnv(text)
				cmd := HarfBuzz(out_name, font, expanded, size)
				cmd.Run()
			}
		}
	}
}

// Adds a bunch of sample texts to the environment so that
// they can be used by callers without any extra definition.
func AddSamples() {
	// Suitable for normal fonts with upper- and lower-case and a
	// range of punctuation.
	os.Setenv("SampleAJS", `AaBbCcDdEeFfGgHhIi
JjKkLlMmNnOoPpQqRr
SsTtUuVvWwXxYyZz -
123456789 $&@!";'?`)

	os.Setenv("Sampleeoy", `ABCDEFGHIJ
KLMNOPQRST
UVWXYZabcd
efghijklmn
opqrstuvwx
yz 1234567
89.0!"$&*)
-[;:'@#,/?`)

	// Suitable for fonts with only upper-case
	// and a good range of punctuation.
	os.Setenv("SampleAKU", `ABCDEFGHIJ
KLMNOPQRST
UVWXYZ 123
456789.0!"
$&*)-;'@/?`)

	// Very minimal. Only upper-case and numbers.
	os.Setenv("SampleMin", `ABCDEFGHIJ
KLMNOPQRST
UVWXYZ 123
4567890`)

	// For fixed-width fonts with upper- and lower-case,
	// this is preferred over AJS.
	os.Setenv("SampleAFK", `AaBbCcDdEe
FfGgHhIiJj
KkLlMmNnOo
PpQqRrSsTt
UuVvWwXxYy
Zz 1234567
89.0!"$&*)
-[;:'@#,/?`)

}

// Add to the environment, names obtained from the font's name table.
func AddEnvFontNames(filename string) {
	r, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer r.Close()

	fon, err := opentype.OpenFont(r)
	if err != nil {
		log.Fatal(err)
	}

	table, ok := fon.Table["name"]
	if !ok {
		log.Fatalf("Mysteriously, there is no name table in %s.", filename)
	}

	name_table := opentype.NewName(table)

	os.Setenv("FontName", NameID(name_table, 4))
	galsample := NameID(name_table, 32008)
	if galsample == "" {
		galsample = os.Getenv("SampleAFK")
	}
	os.Setenv("GalSample", galsample)
}

// About the first suitable NameID in the name table and return it.
func NameID(table *opentype.NameTable, id int) string {
	for _, record := range table.Nrec {
		name := table.DecodeName(&record)
		if name.Value == "" {
			continue
		}
		if int(name.NameID) != id {
			continue
		}
		return name.Value
	}
	return ""
}

func Magick(output, font, text string, size int) *exec.Cmd {
	cmd := exec.Command("convert",
		"-background", "white", "-fill", "black",
		"-font", font,
		"-pointsize", fmt.Sprint(size),
		"label:"+text, output)
	return cmd
}

func Pango(output, font, text string, size int) *exec.Cmd {
	desc := fmt.Sprintf("%s %d", font, size)
	cmd := exec.Command("pango-view",
		"-q", "--output", output,
		"--font", desc,
		"--text", text)
	return cmd
}

func HarfBuzz(output, font, text string, size int) *exec.Cmd {
	cmd := exec.Command("hb-view",
		"--output-file", output,
		"--font-size", fmt.Sprint(size),
		font,
		text)
	return cmd
}
