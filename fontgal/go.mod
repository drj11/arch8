module example.com/fontgal

require (
	example.com/opentype/opentype v0.0.0
	gitlab.com/font8/coff v0.0.0-20221029183915-55542b7510e9 // indirect
)

// Note: this is is another repo!
replace example.com/opentype/opentype => ../../opentype

go 1.14
