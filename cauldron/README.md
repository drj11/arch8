# Cauldron

The Cauldron font has only space, numerals, and the capital letters
(plus a couple of odd sorts).

Amusingly the middle of the letters are laid out in RAM like this:

Where i would expect to see an O, i find a %.
Examination of screenshots reveals that the O in SCORE uses the
same glyph as the numeral 0.
An extremely poor typographic practice dating back to cheap
typewriters.
It saved them 8 bytes.

The Copyright symbol has been moved from its usual position 0x7F
to 0x7E, to make way for a broomstick glyph
(used to indicate how many "lives" you have).
It's the same graphic as the standard ROM font,
so i haven't included it here.

Possibly the 2-cell glyph could be used for KEY U+1F511
